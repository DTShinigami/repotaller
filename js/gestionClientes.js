var clientesObtenidos;

function getClientes() {
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var params="?$filter=Country eq 'Germany'";


  var request= new XMLHttpRequest();
  request.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();

    }
  }
  request.open("GET",url,true);
  request.send();

}
function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);

  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONClientes.value[i].ProductName);
    //tabla
    var nuevaFila = document.createElement("tr");
    var contactName = document.createElement("td");
    contactName.innerText = JSONClientes.value[i].ContactName;
    var city = document.createElement("td");
    city.innerText = JSONClientes.value[i].City;
    var country = document.createElement("td");

    var bandera = JSONClientes.value[i].Country;

    switch (bandera) {
      case "UK":
        bandera = "United-Kingdom";
        country.innerHTML = "<img src='https://www.countries-ofthe-world.com/flags-normal/flag-of-"+bandera+".png'>"
        break;
      case "USA":
        bandera = "United-States-of-America";
        country.innerHTML = "<img src='https://www.countries-ofthe-world.com/flags-normal/flag-of-"+bandera+".png'>"
        break;
      default:
        country.innerHTML = "<img src='https://www.countries-ofthe-world.com/flags-normal/flag-of-"+bandera+".png'>"
    }




    nuevaFila.appendChild(contactName);
    nuevaFila.appendChild(city);
    nuevaFila.appendChild(country);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.innerHTML = "";
  divTabla.appendChild(tabla);
  //alert(JSONClientes.value[0].ProductName);
  }
